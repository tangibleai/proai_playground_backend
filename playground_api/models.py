import jsonfield
from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone


class Game(models.Model):
    created = models.DateTimeField(default=timezone.now)
    user_id = models.TextField(blank=True, null=True)
    game_data = jsonfield.JSONField(blank=True, null=True)
    session_id = models.IntegerField(blank=True, null=True)
    # answer = models.TextField(blank=True, null=True)

    def __str__(self):
        return f"{self.user_id}: {self.created}"


class Profile(models.Model):
    user = models.OneToOneField(
        User, null=True, default=1, on_delete=models.CASCADE, related_name="profile"
    )
    username = models.TextField("Username", null=True, default="Anonymous")
    email = models.EmailField("Email", unique=True)

    def __str__(self):
        return f"{self.user.username} Profile"


class Answer(models.Model):
    # game_id foriegn key
    # session id
    # score =  {rsme}
    created = models.DateTimeField(default=timezone.now)
    session_id = models.IntegerField(blank=True, null=True)
    answer = jsonfield.JSONField(blank=True, null=True)
    game = models.ForeignKey(Game, blank=True, null=True, on_delete=models.CASCADE)
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE, null=True)
    # score =

    def __str__(self):
        return str(self.session_id)
