from django.apps import AppConfig


class SveltApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'playground_api'
