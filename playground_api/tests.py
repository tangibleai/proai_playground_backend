from django.test import TestCase
from django.test import Client
from playground_api.models import Profile
from django.contrib.auth.models import User

# Create your tests here.
class LoginTestCase(TestCase):
    def test_signUp(self):
        c = Client()
        response = c.post(
            "/api/users/",
            {
                "password": "girolabs123",
                "profile": {
                    "username": "francisco@girolabs.com",
                    "email": "francisco@girolabs.com",
                },
            },
            content_type="application/json",
        )
        assert response.status_code == 201

    def test_login(self):
        c = Client()
        User.objects.create_user(
            username="francisco@girolabs.com",
            email="francisco@girolabs.com",
            password="girolabs123",
        )
        response = c.post(
            "/api/token/",
            {"username": "francisco@girolabs.com", "password": "girolabs123"},
            content_type="application/json",
        )
        assert response.status_code == 200

    # def test_login(self):
