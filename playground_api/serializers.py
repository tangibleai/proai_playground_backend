from django.contrib.auth.models import User
from rest_framework import serializers
from playground_api.models import Profile, Answer


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        exclude = [
            "user",
        ]


class AnswerSerializer(serializers.ModelSerializer):

    answer = serializers.JSONField()

    class Meta:
        model = Answer
        fields = ("session_id", "answer", "game", "profile")


class UserSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer()

    class Meta:
        model = User
        fields = (
            "id",
            "first_name",
            "last_name",
            "email",
            "password",
            "is_staff",
            "profile",
        )
        extra_kwargs = {"password": {"write_only": True}}

    def create(self, validated_data):
        profile_data = validated_data.pop("profile")
        user = User.objects.create(
            **validated_data,
            username=profile_data["username"],
            email=profile_data["email"],
        )
        profile = Profile.objects.create(user=user, **profile_data)
        user.set_password(validated_data["password"])
        user.save()
        profile.save()
        return user
