from django.contrib import admin
from .models import Answer, Game, Profile

admin.site.register(Answer)
admin.site.register(Game)
admin.site.register(Profile)
