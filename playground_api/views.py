import numpy as np
from django.contrib.auth.models import User
from playground_api.serializers import AnswerSerializer, UserSerializer
from playground_api.models import Answer, Game, Profile
from rest_framework import status, generics, permissions, viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import permissions
import json

# from proai_playground.settings import DATA_DIR


class SvelteApi(APIView):

    """ """

    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, format=None):

        slope = 2 * np.random.rand() - 1
        intercept = 4 * np.random.rand() + 6
        std_dev = 1.5
        session_id = np.random.randint(1000000000)

        x = [i for i in range(4, 14)]
        x = list(np.array(x))

        y = list(slope * np.array(x) + intercept + np.random.randn(10) * std_dev)

        data = {
            "hello": "Hello world!!",
            "slope": slope,
            "intercept": intercept,
            "x": x,
            "y": y,
            "session_id": session_id,
        }

        return Response(data)

    def post(self, request, format=None):

        data = {}

        if not request.body:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        body = request.data
        session_id = body.get("session_id")
        username = body.get("username")
        answer = body.get("answer")
        array = answer.get("data")

        if session_id == 0:
            session_id = np.random.randint(1000000000)

        # get or create user and profile
        user, checkuser = User.objects.get_or_create(username=username)
        profile, checkprofile = Profile.objects.get_or_create(email=user)
        x = []
        y = []

        for elem in array:
            x.append(elem.get("x"))
            y.append(elem.get("y"))

        data = {
            "slope": answer.get("slope"),
            "intercept": answer.get("intercept"),
            "x": x,
            "y": y,
            "sessionId": session_id,
            "username": username,
        }

        game, check = Game.objects.get_or_create(session_id=session_id, user_id=user.id)

        if check:
            game.game_data = data
            game.save()

        answer = Answer.objects.create(
            session_id=session_id, answer=answer, game=game, profile=profile
        )

        if data:
            return Response(data, status=201)
        return Response(status=status.HTTP_400_BAD_REQUEST)


class UserList(generics.ListCreateAPIView):
    permission_classes = []
    serializer_class = UserSerializer
    queryset = User.objects.all()

    def post(self, request, format=None):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AnswerList(generics.ListAPIView):

    queryset = Answer.objects.all()
    serializer_class = AnswerSerializer
    permission_classes = [permissions.IsAuthenticated]
    filterset_fields = ["session_id"]
