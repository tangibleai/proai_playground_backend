from django.urls import path, include
from rest_framework.routers import DefaultRouter
from rest_framework.authtoken import views
from playground_api.views import AnswerList, SvelteApi, UserList

urlpatterns = [
    path("playground/", SvelteApi.as_view()),
    path('token/', views.obtain_auth_token),
    path("users/", UserList.as_view()),
    path("answers/", AnswerList.as_view())
]
