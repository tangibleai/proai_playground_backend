# Sprint Planning for John sep 19, 2021

## ML Game

- [x] clean django project repo `proai_playground_backend`
- [x] clean svelte project repo `proai_playground_frontend`
- [x] model to hold `Answer(models.Model)`
- [x] get request to retrieve data
- [ ] make min and max slope values global javascript variables *1*
- [ ] make min and max intercept values global javascript variables *.5*
- [x] clean up the data to have x and y as top level keys *.5*
- [x] fix svelte app to use correct keys to retrieve data points *1*
- [x] populate json response with `x = list(np.array([4,5,6...,14]))` *4*
- [x] use existing slope (+0.5) and intercept (+3) to generate 10 random y values using `y = list(0.5*np.array(x) + 3.0 + np.random.randn(10))`,  data with each GET/POST request *2*

- [x] new key called `slope` that returns the fixed 0.5 slope value *.5*
- [x] randomize the slope value so it's no longer .5: python view generate random slope value between -1 and 1 `slope = 2*np.random.rand() - 1` and return slope within json response *1*
- [x] change the slope in the django api view (instead of .5) to use new random slope value when it creates y values
- 
- [x] new key called `intercept` returns fixed value of 3 along with random data x and y
- [x] python view generates new random `intercept` value between 3 and 13 `intercept = 10*np.random.rand() + 3`
- [x] python view returns data {x, y, slope, intercept} that uses randomly generated slope and intercept to generate data
- [ ] user hits start button and api generates random userid np.random.randint(1, 1_000_000_000_000) and store in User model also in cookie on users browser and also returns x,y,slope,intercept,userid data for the new game (javascript stores userid in cookie)
- [ ] user hits submit button to check their answer
- [ ] django model for Problem(models.Model) with jsonfield (or TextField) to store the data that was returned to the front end, along with timestamp and userid 
- [ ] use new random slope
- [ ] data returned contains the slope and the intercept plus 10 points
- [ ] svelte app that detects range of data for min and max values in javascript

## Svelte frontend for qary chatbot
