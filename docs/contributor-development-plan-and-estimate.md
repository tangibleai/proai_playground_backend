## Machine Learning Playground Prototype

### Scope of Work

Tasks are described under Deliverables, below.  We will provide SSH credentials to VMs (droplets) on DigitalOcean or an account on DigitalOcean, as needed.

#### Open Source Code

All code developed for the project is open source. Developers and contractors are free to copy and reuse elsewhere for any purpose (MIT license) the code they write for this contract. This includes all devops bash scripts, docker files and scripts, gitlab CICD config files, frontend and backend javascript and python code. In addition unrelated software repostories are also provided for reuse elsewhere as you see fit. 

All devliverables should be provided by the developer as gitlab merge requests (pull requests) to the Exisitng Prototype gitlab repos listed above.

#### Proprietary Data

All company data (user data, financials, server logins/passwords) must be protected as propriety sensitive data (we will be providing the developer with access to our Digital Ocean account).

#### Tangible AI support

John is available for 30 minutes a week and by Slack messaging to help understand the existing codebase (backend and frontend) and help debug. John and Hobson are also available at any time to provide access to cloud compute resources needed to deploy and test the app (e.g. Digital Ocean droplets). We will also provide you access to any other resources you need (such as developer privileges on the gitlab repos that you would like to contribute to directly).

#### Existing Prototypes

- [proai_playground_backend](https://gitlab.com/tangibleai/proai_playground_backend)
- [proai_playground_frontend](https://gitlab.com/tangibleai/proai_playground_frontend)
- old prototype [in interactive IDE](https://svelte.dev/repl/25c5a42977024963accdc3cec81e252e)
- an old unrelated Django app with deployment automation, user authentication, letsencrypt integration, and e-mail sending libraries: [tanbot](https://gitlab.com/tangibleai/tanbot)

### Deliverables

All devliverables should be provided within pull requests (merge requests) to the Exisitng Prototype gitlab repos listed above.

1. Deploy the existing prototype: 2 weeks, 18hr?

- 4hr: Deploy backend to Digital-Ocean using Docker (we provide you with ssh credentials to an EC2 instance containing the Docker daemon and Docker-compose application)
- 4hr: Deploy frontend to Digital-Ocean using Docker or sveltekit or as part of the backend deployment
- 10hr: Add gitlab-ci.yml to [proai_playground_backend](https://gitlab.com/tangibleai/proai_playground_backend) that auto-deploys frontend and backend

2. Add user authentication: 2 weeks, 24hr?

- 4hr: Svelte login component (form with email address field, password field, and )
- 4hr: backend login functionality (`models.Profile(django.contrib.auth.User)` with email field) with 1 unittest, tutorial video [here](https://youtu.be/q4jPR-M0TAQ)
- 4hr: Svelte signup component (form with email and create password field)
- 4hr: backend signup functionality with unittest (no e-mail verification required)
- 4hr: email verification feature (using our SendGrid account and example python scripts we provide)

3. Display table of users' "homework" submissions, 1 week, 14 hr?

- 2hr: remove live update of RMSE and line within linear regression
- 4hr: Svelte table component that works with at least 10 columns and 100 rows of float & string values
- 4hr: Populate svelte table with query of the logged in user's previous submissions (slope, intercept, RMSE) for that session/game
- 4hr: after [Submit] button is clicked update displayed table with a new row containing (slope, intercept, RMSE) -- backend functionality is already implemented

### Stretch goals

- 2hr: frontend Svelte password reset component
- 4hr: backend password reset functionality integrated into models.py and sendgrid 

