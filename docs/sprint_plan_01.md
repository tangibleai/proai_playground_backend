# proai_playground live sprint plan

- [ ] create diagram / map proai infrastructure
- [ ] create diagram / map proai network

what services

- [X] manually deploy to digitalOcean Debian server
    * [x] deploy local backend
    * [x] deploy local frontend

- [ ] manually deploy to digitalOcean Debian server
    - [x] add nginx service
    - [x] add gunicorn.service

    - [ ] add commands to script - backend
        - [x] t - provision server - spin up server
        - [x] b - rsnc code to server
        - [x] b - activate environment / install dependencies
        - [ ]

     - [ ] add commands to script - frontend
        - [ ] b - rsnc code to server
        - [ ] b - activate environment / install dependencies

    - [ ] custom domain && certbot
        - [X] `play.qary.ai` custom domain
        - [ ] SLL certs

- [x] add new sprint plan push to main
