#!/bin/sh

# Docker related variables
echo IMAGE=$CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME >> .env
echo WEB_IMAGE=$IMAGE:web  >> .env
echo NGINX_IMAGE=$IMAGE:nginx  >> .env
echo CI_REGISTRY_USER=$CI_REGISTRY_USER   >> .env
echo CI_JOB_TOKEN=$CI_JOB_TOKEN  >> .env
echo CI_REGISTRY=$CI_REGISTRY  >> .env


# Database variables
echo POSTGRES_DB=${POSTGRES_DB} >> .env
echo POSTGRES_PASSWORD=${POSTGRES_PASSWORD} >> .env
echo POSTGRES_USER=${POSTGRES_USER} >> .env

# Django settings variables
echo DEBUG=$DEBUG >> .env
echo SECRET_KEY=$SECRET_KEY >> .env
if [ -n "$DJANGO_ALLOWED_HOSTS" ] ; then
    echo "using existing DJANGO_ALLOWED_HOSTS='$DJANGO_ALLOWED_HOSTS'"
else
    export DJANGO_ALLOWED_HOSTS="*"
    echo "Creating DJANGO_ALLOWED_HOSTS='$DJANGO_ALLOWED_HOSTS'"
fi
echo DJANGO_ALLOWED_HOSTS=$DJANGO_ALLOWED_HOSTS  >> .env
echo SQL_DATABASE=$SQL_DATABASE >> .env
echo SQL_ENGINE=${SQL_ENGINE} >> .env
echo SQL_HOST=$SQL_HOST >> .env
echo SQL_PASSWORD=$SQL_PASSWORD >> .env
echo SQL_PORT=$SQL_PORT >> .env
echo SQL_USER=$SQL_USER >> .env

# Nginx settings
echo NGINX_BACKEND_HOST=${NGINX_BACKEND_HOST} >> .env
echo NGINX_FRONTEND_HOST=${NGINX_FRONTEND_HOST} >> .env

# other variables
echo API_BASE=${API_BASE} >> .env
echo DATABASE=postgres >> .env
echo SSH_HOST=${SSH_HOST} >> .env
echo SSH_USER=${SSH_USER} >> .env

VENV_ACTIVATE_PATH=".venv/bin/activate"
if [ -f "$VENV_ACTIVATE_PATH" ] ; then
    echo "" >> .venv/bin/activate
    echo "source .env" >> .venv/bin/activate
fi
