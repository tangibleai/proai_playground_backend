### Build and install packages
FROM python:3.9 as build-python

WORKDIR /usr/src/app

RUN apt-get -y update \
  && apt-get install -y gettext \
  # Cleanup apt cache
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

# Install Python dependencies
COPY requirements.txt /usr/src/app/
RUN pip install -r requirements.txt

### Final image
FROM python:3.9-slim

RUN groupadd -r app && useradd -r -g app app

ENV APP_HOME=/home/app/web
RUN mkdir -p $APP_HOME
RUN mkdir -p $APP_HOME/static
WORKDIR $APP_HOME

RUN apt-get update \
  && apt-get install -y \
  && apt-get install python3-dev libpq-dev netcat -y \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

COPY --from=build-python /usr/local/lib/python3.9/site-packages/ /usr/local/lib/python3.9/site-packages/
COPY --from=build-python /usr/local/bin/ /usr/local/bin/

# copy entrypoint.sh
COPY ./entrypoint.sh $APP_HOME

# copy project
COPY . $APP_HOME

# chown all the files to the app user
RUN chown -R app:app $APP_HOME

# change to the app user
USER app

ENV PYTHONUNBUFFERED 1

EXPOSE 8000

# run entrypoint.sh
ENTRYPOINT ["/home/app/web/entrypoint.sh"]

CMD gunicorn proai_playground.wsgi:application --bind 0.0.0.0:8000 --workers 4
