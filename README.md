
## Developer install

First retrieve a copy of the source code for django-api:

```bash
git clone git@gitlab.com:tangibleai/proai_playground_backend.git
cd proai_playground_backend
```

### Conda Environment Manager

Then, install and use the `conda` python package manager within the [Anaconda](https://www.anaconda.com/products/individual#Downloads) software package.

```bash
conda update -y -n base -c defaults conda
conda create -y -n djangoapienv 'python==3.7.10'
conda activate djangoapienv
conda install -y jupyter
pip install -r requirements.txt
```

### Venv Environment Manager

```bash
sudo apt install python3-dev libpq-dev
python3.7 -m venv .venv
pip install -r requirements.txt
```

#### Run locally

```bash
python manage.py runserver --insecure
```



## Steps to set up an Ubuntu 20.04 Server on a DigitalOcean Droplet

Get your DigitalOcean Personal Access Token
https://docs.digitalocean.com/reference/api/create-personal-access-token/
```bash
export DIGITAL_OCEAN_ACCESS_TOKEN='YourDIGITAL_OCEAN_ACCESS_TOKEN'
```

**Create a new Droplet** *change name and/or region/size/image*
```bash
curl -X POST \
     -H 'Content-Type: application/json' \
     -H 'Authorization: Bearer '$DIGITAL_OCEAN_ACCESS_TOKEN'' \
     -d '{"name":"django-api","region":"sfo2","size":"s-1vcpu-1gb","image":"ubuntu-20-04-x64"}' \
     "https://api.digitalocean.com/v2/droplets"
```
**Check the status**  \*It will take a few minutes for the droplet be created and return`"active"`.
<br />
install jq
```bash
sudo apt install jq
```
check the status \**change name*
```bash
curl \
     -H 'Content-Type: application/json' \
     -H 'Authorization: Bearer '$DIGITAL_OCEAN_ACCESS_TOKEN'' \
     "https://api.digitalocean.com/v2/droplets?name=django-api" \
   | jq '.droplets[0].status'
```

Login with IP address and password emailed to you.
```bash
ssh root@YOUR_INSTANCE_IP
```
Change password when prompted.

### configure new server

1. ##### Install software updates
```bash
apt-get update && apt-get upgrade
```

2. ##### Add limited user
add a user  \**I set a password and left the rest blank.*
```bash
adduser your_username
```
add user to sudo
```bash
adduser your_username sudo
```
`exit` then login as user
```bash
ssh your_username@YOUR_INSTANCE_IP
```
*you should now be logged in as user not root*

3. ##### Setup SSH key-based authentication
create directory
```bash
mkdir -p ~/.ssh
```
From local terminal copy your public key to your new server's .ssh/authorized_keys.
```bash
# this command is to be ran from a local terminal
scp ~/.ssh/id_rsa.pub your_username@YOUR_INSTANCE_IP:~/.ssh/authorized_keys
```
Back on new server check that authorized_keys has been created.
```bash
# back on the new server
ls .ssh
```
Set permissions for the ssh directory to where owner of the dir has read/write/execute permissions of dir and owner of the files will have read/write permissions on those files.
```bash
sudo chmod 700 ~/.ssh/
sudo chmod 600 ~/.ssh/*
```
you can `exit` or in new terminal test login without password
```bash
ssh your_username@YOUR_INSTANCE_IP
```
now we want to not allow root logins:
+ change PermitRootLogin to no --> PermitRootLogin no
+ comment out PasswordAuthentication and set to no --> # PasswordAuthentication no
```bash
sudo nano /etc/ssh/sshd_config
```
\**like so*
```bash
    GNU nano 4.8                              /etc/ssh/sshd_config
    ...
    #LoginGraceTime 2m
    PermitRootLogin no
    #StrictModes yes
    #MaxAuthTries 6
    #MaxSessions 10
    ...
    # To disable tunneled clear text passwords, change to no here!
    # PasswordAuthentication no
    #PermitEmptyPasswords no
    ...

```
`ctrl + x` `y` `enter`
+ restart ssh server
```bash
sudo systemctl restart sshd
```

4. ##### Set up a firewall with UFW
Install UFW
```bash
sudo apt-get install ufw
```
+ setup a few rules
+ enable firewall
+ check status
```bash
sudo ufw default allow outgoing
sudo ufw default deny incoming
sudo ufw allow ssh
sudo ufw allow 8000
sudo ufw enable
sudo ufw status
```

5. ##### Connect with Gitlab and clone repo
Generate the SSH
```bash
ssh-keygen -b 4096
# press enter for defaults
```
Copy your public SSH key
```bash
cat ~/.ssh/id_rsa.pub
```
+ navigate to https://gitlab.com -> preferences -> SSH Keys
+ paste key -> Add key
+ clone the repository
```bash
git clone git@gitlab.com:tangibleai/django-api.git
cd proai_playground
```
6. ##### Create environment
+ install python
+ create virtual environment
+ activate virtual environment
+ install requirements
```bash
sudo apt-get install python3-pip
sudo apt-get install python3-venv
python3 -m venv apienv
source apienv/bin/activate
pip install -r requirements.txt
```

7. ##### Test localhost
add 'YOUR_INSTANCE_IP' to ALLOWED_HOSTS
```bash
nano proai_playground/settings.py
```
copy configuration file to server
```bash
sudo cp config.json /etc/config.json
nano /etc/config.json # add SECRET_KEY
```
+ migrate
+ run tests
+ collect static files
+ run server localhost
```bash
python manage.py migrate
python manage.py test -v2
python manage.py collectstatic
python manage.py runserver 0.0.0.0:8000
```
\**in your browser navigate to http://YOUR_INSTANCE_IP:8000/api/recommendation/*
+ quit server localhost
`Ctrl + C`

8. ##### Install and configure Apache
install Apache
```bash
cd
sudo apt-get install apache2
sudo apt-get install libapache2-mod-wsgi-py3
```
configure apache server
+ copy the apache default config file
```bash
cd /etc/apache2/sites-available/
sudo cp 000-default.conf django-api.conf
```
+ add to new config file
```bash
sudo nano django-api.conf
```
\* before the closing virtual host tag add
```bash
    GNU nano 4.8                  /etc/apache2/sites-available/django-api.conf
    ...

    Alias /static /home/user/django-api/static
    <Directory /home/user/django-api/static>
    Require all granted
    </Directory>

    <Directory /home/user/django-api/proai_playground>
        <Files wsgi.py>
            Require all granted
        </Files>
    </Directory>

    WSGIScriptAlias / /home/user/django-api/proai_playground/wsgi.py
    WSGIDaemonProcess proai_playground python-path=/home/user/django-api python-home=/home/user/django-api/apienv
    WSGIProcessGroup proai_playground

```
+ enable site through apache
+ disable default site
```bash
cd
sudo a2ensite django-api
sudo a2dissite 000-default.conf
```
+ change ownership and permissions, give apache access to db
```bash
sudo chown :www-data django-api/db.sqlite3 # change owner of db to apache
sudo chmod 664 django-api/db.sqlite3 # change permission
sudo chown :www-data django-api # change ownership of project
```
+ dissallow port 8000
+ allow http traffic
```bash
sudo ufw delete allow 8000
sudo ufw allow http/tcp
```
restart apache server
```bash
systemctl reload apache2
```

9. ##### Setup a custom domain name
How to add a subdomain to your Domain
+ follow these instructions
+ https://docs.digitalocean.com/products/networking/dns/how-to/add-subdomain/
+ add 'YOUR_DOMAIN_NAME' to ALLOWED_HOSTS
```bash
sudo nano django-api/proai_playground/settings.py # ALLOWED_HOSTS = ['onow.qary.ai', ...
```

10. ##### Enable HTTPS Let's Encrypt
\**if more info is needed* https://certbot.eff.org/lets-encrypt/ubuntufocal-apache
+ ensure that snapd is up to date
+ Install Certbot
+ prepare the Certbot command
```bash
sudo snap install core; sudo snap refresh core
sudo snap install --classic certbot
sudo ln -s /snap/bin/certbot /usr/bin/certbot
```
+ uncomment ServerName
+ change ServerName YOUR_CUSTOM_DOMAIN_NAME
+ comment out the 3 WSGI lines \**like so*
```bash
sudo nano /etc/apache2/sites-available/django-api.conf
```
```bash
    GNU nano 4.8                  /etc/apache2/sites-available/django-api.conf
    ...
    # However, you must set it for any further virtual host explicitly.
    ServerName YOUR_CUSTOM_DOMAIN_NAME # onow.qary.ai

    ServerAdmin webmaster@localhost
    ...

    Alias /static /home/user/django-api/static
    <Directory /home/user/django-api/static>
    Require all granted
    </Directory>

    <Directory /home/user/django-api/proai_playground>
        <Files wsgi.py>
            Require all granted
        </Files>
    </Directory>

    #WSGIScriptAlias / /home/user/django-api/proai_playground/wsgi.py
    #WSGIDaemonProcess proai_playground python-path=/home/user/django-api python-home=/home/user/django-api/apienv
    #WSGIProcessGroup proai_playground

```
+ run command
+ `enter` email when promted
+ `A` to agree with terms of service
+ `N` to share email
+ `enter` to select name
```bash
sudo certbot --apache

```
+ these configurations have now been copied to a new config file but we must delete these lines here. \**delete these lines*
```bash
sudo nano /etc/apache2/sites-available/django-api.conf
```
```bash
    Alias /static /home/user/django-api/static
    <Directory /home/user/django-api/static>
    Require all granted
    </Directory>

    <Directory /home/user/django-api/proai_playground>
        <Files wsgi.py>
            Require all granted
        </Files>
    </Directory>

    #WSGIScriptAlias / /home/user/django-api/proai_playground/wsgi.py
    #WSGIDaemonProcess proai_playground python-path=/home/user/django-api python-home=/home/user/django-api/apienv
    #WSGIProcessGroup proai_playground
```
+ open the new config file
+ uncomment the 3 WSGI lines \**like so*
```bash
sudo nano /etc/apache2/sites-available/django-api-le-ssl.conf
```
```bash
    GNU nano 4.8                  /etc/apache2/sites-available/django-api-le-ssl.conf
    ...
    # However, you must set it for any further virtual host explicitly.
    ServerName YOUR_CUSTOM_DOMAIN_NAME # onow.qary.ai

    ServerAdmin webmaster@localhost
    ...

    Alias /static /home/user/django-api/static
    <Directory /home/user/django-api/static>
    Require all granted
    </Directory>

    <Directory /home/user/django-api/proai_playground>
        <Files wsgi.py>
            Require all granted
        </Files>
    </Directory>

    WSGIScriptAlias / /home/user/django-api/proai_playground/wsgi.py
    WSGIDaemonProcess proai_playground python-path=/home/user/django-api python-home=/home/user/django-api/apienv
    WSGIProcessGroup proai_playground

```
+ allow https traffic
+ restart apache
```bash
sudo ufw allow https
sudo service apache2 restart
```
setup cron job to auto renew certbot
+ select editor
```bash
sudo crontab -e
1 # nano
```
add this to the crontab
```bash
30 4 1 * * sudo certbot renew --quiet
```
