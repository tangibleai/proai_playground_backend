# variables.tf

output "instance_id" {
  description = "ID of the digitalocean_droplet instance"
  value       = digitalocean_droplet.my-instance-name.id
}

output "ipv4_address" {
  description = "Public IP address of the digitalocean_droplet instance"
  value       = digitalocean_droplet.my-instance-name.ipv4_address
}


# more info on Environment Variables: https://www.terraform.io/language/values/variables
# Terraform searches the environment for variables named TF_VAR_ followed by the name of a declared variable.


# in ~/.secrets add: export TF_VAR_<variable_name>="<value>"
variable "TERRAFORM_TOKEN" {}
