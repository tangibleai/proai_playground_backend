#!/bin/bash

ssh -i ~/.ssh/digitalocean/keys/terraform-ssh-key.pem root@change_me -o StrictHostKeyChecking=no <<HERE

    sleep 3

    sudo apt-get -y update && sudo apt-get -y upgrade
    sudo apt-get install -y zip
    unzip proai_playground_backend.zip
    cd proai_playground_backend
    source .env

    sudo apt-get install -y nginx
    sudo systemctl start nginx
    sudo apt-get install -y vim
    sudo apt-get install -y bash-completion
    sudo apt-get install -y wget
    sudo apt-get install -y curl
    sudo apt-get install -y gnupg
    sudo apt-get install -y apt-transport-https zip
    sudo apt-get install -y software-properties-common
    sudo apt-get install -y erlang
    sudo apt-get install -y python3-pip
    sudo apt-get install -y python3-dev
    sudo apt-get install -y python3-testresources
    sudo apt-get install -y python3-venv
    sudo apt-get install -y gunicorn

    pip install -r requirements.txt

    chmod -R 755 /root/
    sudo rm /etc/nginx/sites-enabled/default
    # chmod +x terraform/scripts/setup_nginx.sh
    # terraform/scripts/setup_nginx.sh
    sudo cp etc/gunicorn.service /etc/systemd/system/gunicorn.service
    sudo systemctl enable --now gunicorn.service
    sudo systemctl daemon-reload
    sudo cp etc/proai_playground_backend_gunicorn.nginx /etc/nginx/sites-enabled/proai_playground_backend_gunicorn.nginx
    sudo systemctl enable --now nginx
    sudo systemctl daemon-reload
    sudo systemctl restart nginx
    sudo systemctl restart gunicorn.socket gunicorn.service

HERE
